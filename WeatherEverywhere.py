import telegram
from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler, Filters
from requests import get
from bs4 import BeautifulSoup
import json
import sys
import traceback

GEO_URL = " https://geocode-maps.yandex.ru/1.x/"
PICTURE_URL = "https://www.bing.com/images/search"
GEO_KEY = \
    "AIWpJ1oBAAAAzBD1fwIASt3tzXE-RXxr0d4XYD3wssVCgTkAAAAAAAAAAACtasiMN62epHHwY5wnImW5cKPyVw=="

WEATHER_URL = "http://api.openweathermap.org/data/2.5/weather"
WEATHER_KEY = "1bbe90843ded0259d3fd625e9037ad83"

SUNNY_CAT = "http://hq-oboi.ru/photo/kotik_kak_pushistyy_komochek_1920x1200.jpg"
SAD_CAT = "http://99px.ru/sstorage/56/2017/03/image_561603170403223174732.jpg"

CITIES = {}
BOT_MOTHER_ID = 277001697
SEND_IMAGE_INF = {}

#
# Start, help, errors, incorrect user requests handlers
#


def start_help(bot, update):
    message = "Привет!\nМеня зовут WeatherEverywhere, и я могу рассказать тебе," + \
              " какая сейчас погода в любом уголке нашей планеты." + \
              " Я понимаю вот такие команды:\n\n/weather _название места_ -- " + \
              "узать текущую погоду в данном месте\n" + "/cat -- кинуть котика\n" + \
              "/help -- вывести эту инструкцию\n"
    bot.send_message(chat_id=update.message.chat_id, text=message, parse_mode="Markdown")


def answer_to_text_message(bot, update):
    bot.send_message(chat_id=update.message.chat_id,
                     text="Прости, но мне нечего тебе на это сказать " + chr(0x1F910) +
                          "\nМогу только порадовать тебя котиком:")
    send_sunny_cat(bot, update)


def error_callback(bot, update, error):
    message = "Mom, I have some troubles :(\n"
    if update.message is not None:
        message += "chat_id: {}\n".format(update.message.chat_id)
        message += "user message: {}\n".format(update.message.text)
    message += "{}: ".format(type(error).__name__)
    message += "{}\n".format(error)
    ex_type, ex, tb = sys.exc_info()
    message += "Traceback:\n" + traceback.format_tb(tb)[0]
    bot.send_message(chat_id=BOT_MOTHER_ID, text=message)


def unknown_command(bot, update):
    answer_to_text_message(bot, update)


def yes_answer(bot, update):
    chat_id = update.message.chat_id
    if (chat_id in SEND_IMAGE_INF.keys()
        and SEND_IMAGE_INF[chat_id]['photo_message_id'] is not None):
        SEND_IMAGE_INF[chat_id]['photo_message_id'] = None
        message = "Ура! "+chr(0x1F60A)
        bot.send_message(chat_id=chat_id, text=message)
    else:
        answer_to_text_message(bot, update)


def no_answer(bot, update):
    chat_id = update.message.chat_id
    if (chat_id in SEND_IMAGE_INF.keys()
        and SEND_IMAGE_INF[chat_id]['photo_message_id'] is not None):
        bot.deleteMessage(chat_id=chat_id,
                          message_id=SEND_IMAGE_INF[chat_id]['photo_message_id'])
        SEND_IMAGE_INF[chat_id]['photo_message_id'] = None
        bot.send_message(chat_id=chat_id, text="Я поищу для тебя ещё!")
        send_suitable_image(bot, update, chat_id=chat_id)
    else:
        answer_to_text_message(bot, update)


class YesFilter(telegram.ext.filters.BaseFilter):
    def filter(self, message):
        return message.text == "Да"


class NoFilter(telegram.ext.filters.BaseFilter):
    def filter(self, message):
        return message.text == "Нет"

#
# Image searching
#


def search_image(query):
    req = get(PICTURE_URL, params={'q': query, 'FORM': 'HDRSC2'})
    assert req.status_code == 200, 'request failed'
    soup = BeautifulSoup(req.text, "lxml")
    pictures = soup.findAll('a', attrs={'class': "thumb"})
    for elem in pictures:
        yield elem['href']


def send_image(bot, update, query, chat_id=None):
    if chat_id is None:
        chat_id = update.message.chat_id
    bot.send_chat_action(chat_id=chat_id, action=telegram.ChatAction.UPLOAD_PHOTO)
    # pictures = filter(lambda x: get_image_size(x) < MAX_IMAGE_SIZE, search_image(query))
    pictures = search_image(query)
    SEND_IMAGE_INF.update({
        chat_id: {
            "photo_message_id": None,
            "query": query,
            "pictures": pictures}
    })
    send_suitable_image(bot, update, chat_id=chat_id)


def send_suitable_image(bot, update, chat_id=None):
    if chat_id is None:
        chat_id = update.message.chat_id
    not_done = True
    query = SEND_IMAGE_INF[chat_id]['query']
    while not_done:
        try:
            pictures = SEND_IMAGE_INF[chat_id]["pictures"]
            photo = (next(pictures))
            keyboard = [[telegram.KeyboardButton("Да"), telegram.KeyboardButton("Нет")]]
            reply_markup = telegram.ReplyKeyboardMarkup(keyboard, one_time_keyboard=True,
                                                        resize_keyboard=True)

            photo_message = bot.sendPhoto(chat_id, photo=photo,
                                          caption="Тебе нравится эта картинка?",
                                          reply_markup=reply_markup)
            SEND_IMAGE_INF.update({
                chat_id: {
                    "photo_message_id": photo_message.message_id,
                    "pictures": pictures,
                    "query": query
                }

            })
            not_done = False
        except (telegram.error.BadRequest, telegram.error.TimedOut):
            pass
        except StopIteration:
            bot.send_message(chat_id,
                             text=("Тут должна была быть картинка по запросу:\n{}\n" +
                                   "Но мы с котиком не смогли найти подходящей, извини :("
                                   ).format(query))
            send_sad_cat(bot, update, chat_id)
            not_done = False

#
# Cats
#


def send_sunny_cat(bot, update, chat_id=None):
    if chat_id is None:
        chat_id = update.message.chat_id
    bot.sendPhoto(chat_id, photo=SUNNY_CAT)
    send_poem(bot, update, "000", chat_id)


def send_sad_cat(bot, update, chat_id=None):
    if chat_id is None:
        chat_id = update.message.chat_id
    bot.sendPhoto(chat_id, photo=SAD_CAT)

#
# Weather command handlers
#


def weather(bot, update, args):
    city_name = ' '.join(args)
    if city_name == "":
        message = "Чтобы узнать погоду, нужно указать название места, например, вот так:\n"
        message += "/weather Москва\n"
        bot.send_message(chat_id=update.message.chat_id,
                         text=message)
    else:
        get_coordinates(bot, update, city_name)


def get_coordinates(bot, update, query):
    bot.send_chat_action(chat_id=update.message.chat_id, action=telegram.ChatAction.FIND_LOCATION)
    req = get(GEO_URL, params={'geocode': query,
                               'key': GEO_KEY, 'results': 7, 'format': 'json'})
    dic = json.loads(req.text)
    if len(dic['response']['GeoObjectCollection']['featureMember']) == 0:
        bot.send_message(chat_id=update.message.chat_id,
                         text="Прости, но мы с котиком не нашли на нашей планете место {}".format(
                             query))
        send_sad_cat(bot, update)
    elif len(dic['response']['GeoObjectCollection']['featureMember']) == 1:
        value = dic['response']['GeoObjectCollection']['featureMember'][0]
        lon, lat = str(value['GeoObject']['Point']['pos']).split(" ")
        city = {
                'description': value['GeoObject']['metaDataProperty']['GeocoderMetaData']['text'],
                'lon': lon,
                'lat': lat,
                'query': query
                }
        reply_current_weather(bot, update, city, update.message.chat_id)
    else:
        cities = []
        keyboard = []
        message = ("По запросу {} я нашёл несколько"
                   + " прекрасных уголков на нашей планете:\n\n").format(query)
        for index, value in enumerate(dic['response']['GeoObjectCollection']['featureMember']):
            lon, lat = str(value['GeoObject']['Point']['pos']).split(" ")
            cities.append({
                'description': value['GeoObject']['metaDataProperty']['GeocoderMetaData']['text'],
                'lon': lon,
                'lat': lat,
                'query': query
            })
            keyboard.append([
                telegram.InlineKeyboardButton(
                    "{}. {}".format(index + 1, cities[index]['description']), callback_data=index)])
            message += "{}. {} ({}, {})\n".format(index + 1, cities[index]['description'],
                                                  lon, lat)
        message += "\nПогода в каком из них тебе интересна?"
        reply_markup = telegram.InlineKeyboardMarkup(keyboard)
        global CITIES
        CITIES.update({update.message.chat_id: cities})
        bot.send_message(chat_id=update.message.chat_id, text=message,  reply_markup=reply_markup)


def reply_current_weather(bot, update, city, chat_id=None):
    if chat_id is None:
        chat_id = update.message.chat_id
    bot.send_chat_action(chat_id=chat_id, action=telegram.ChatAction.FIND_LOCATION)
    curr_weather = get_current_weather(city['lat'], city['lon'])
    if curr_weather['wind_type'] == '':
        curr_weather['wind_type'] = 'ветер'
    message = "Сегодня в прекрасном уголке " + \
              "нашей планеты под названием {} ({}) {}".format(
                  city['query'], city['description'],
                  curr_weather['weather_type'])
    if curr_weather['weather_type'] != curr_weather['clouds_type']:
        message += ", {}\n".format(curr_weather['clouds_type'])
    else:
        message += "\n"
    message += ("температура воздуха: {} °C\n" +
                "относительная влажность: {}%\n" +
                "{}: {} м/с\n" +
                "атмосферное давление: {} " +
                "мм ртутного столба").format(curr_weather['temperature'],
                                             curr_weather['humidity'],
                                             curr_weather['wind_type'],
                                             curr_weather['wind_value'],
                                             curr_weather['pressure'])
    bot.send_message(chat_id, text=message)
    send_poem(bot, update, curr_weather['weather_code'], chat_id=chat_id)
    send_image(bot, update,
               city['description'] + ' ' + curr_weather['weather_type'], chat_id=chat_id)


def button(bot, update):
    '''
    This is a handler for InlineKeyboard updates, which may appear after get_coordinates
    '''
    query = update.callback_query
    chat_id = query.message.chat_id
    city = CITIES[chat_id][int(query.data)]
    reply_current_weather(bot, update, city, chat_id)


def convert_hPa_to_mmHg(pressure):
    hPa_to_mmHg = 0.75006375541921
    return int(float(pressure) * hPa_to_mmHg)


def get_rus_term(term):
    with open('./weather_types.txt', 'r') as f:
        weather_dict = json.loads(f.readlines()[0])
        try:
            rus_term = weather_dict[term]
        except KeyError:
            rus_term = term
    return rus_term


def get_current_weather(lat, lon):
    answer = {}
    req = get(WEATHER_URL, params={'APPID': WEATHER_KEY,
                                   'lat': lat, 'lon': lon, 'units': 'metric',
                                   'mode': 'xml'})
    soup = BeautifulSoup(req.text, 'lxml')
    answer.update({
        'weather_type': get_rus_term(soup.weather['value']),
        'weather_code': soup.weather['number'],
        'temperature': soup.temperature['value'],
        'temperature_min': soup.temperature['min'],
        'temperature_max': soup.temperature['max'],
        'humidity': soup.humidity['value'],
        'wind_type': get_rus_term(soup.wind.speed['name'].lower()),
        'wind_value': soup.wind.speed['value'],
        'clouds_type': get_rus_term(soup.clouds['name'].lower()),
        'last_update': soup.lastupdate['value'],
        'pressure': convert_hPa_to_mmHg(soup.pressure['value'])
    })
    return answer

#
# Poem handler
#


def send_poem(bot, update, weather_code, chat_id=None):
    if chat_id is None:
        chat_id = update.message.chat_id
    with open("./poems.txt", "r") as poems_file:
        with open("./codes_to_poems.txt", "r") as codes_file:
            poems = "".join(poems_file.readlines())
            poems = json.loads(poems)
            codes = "".join(codes_file.readlines())
            codes = json.loads(codes)
    title = codes[weather_code]
    poem = poems[title]
    message = ""
    if weather_code != "000":
        message += "Пока я ищу для тебя красивую картинку, почитай это чудесное стихотворение:\n\n"
    message += '*"{}"*\n\n{}\n\n{}'.format(title, poem['poem'],
                                           poem['author'])
    bot.send_message(chat_id=chat_id, text=message, parse_mode="Markdown")

#
# Main
#


def main():
    updater = Updater(token='498800956:AAHwPz5Lb0HnmyqIQZWeBVuGXKyq2puVj-E')

    dispatcher = updater.dispatcher

    weather_handler = CommandHandler('weather', weather, pass_args=True)
    dispatcher.add_handler(weather_handler)

    start_handler = CommandHandler('start', start_help)
    dispatcher.add_handler(start_handler)

    help_handler = CommandHandler('help', start_help)
    dispatcher.add_handler(help_handler)

    cat_handler = CommandHandler('cat', send_sunny_cat)
    dispatcher.add_handler(cat_handler)

    updater.dispatcher.add_handler(telegram.ext.CallbackQueryHandler(button))

    yes_filter = YesFilter()
    updater.dispatcher.add_handler(MessageHandler(Filters.text & yes_filter, yes_answer))

    no_filter = NoFilter()
    updater.dispatcher.add_handler(MessageHandler(Filters.text & no_filter, no_answer))

    text_message_handler = MessageHandler(Filters.text, answer_to_text_message)
    dispatcher.add_handler(text_message_handler)

    unknown_command_handler = MessageHandler(Filters.command, unknown_command)
    dispatcher.add_handler(unknown_command_handler)

    dispatcher.add_error_handler(error_callback)

    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
