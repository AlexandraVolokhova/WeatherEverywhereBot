# WeatherEverywhere

@WeatherEverywhere -- username in Telegram

This bot can say you what the current weather is in any place in the world. And send you a cat :3

## Description:

For getting the weather just say to bot:

```
/weather name_of_the_place
```

If bot find several places maching name_of_the_place, he will offer your a list of them to choose appropriate.
Having found the appropriate place, bot gives you an information about the current weather, a poem, connected with this weather,
and an image, that bot found in attempt to search for a picture of this place with the current weather. 
If the image doesn't satisfy you, bot will try to find another one.

Moreover, bot can send you a cat, if you type:

```
/cat
```

Just for fun.

## Library:

I use this library for my bot: https://github.com/python-telegram-bot/python-telegram-bot

## APIs:

I use the following APIs:

https://geocode-maps.yandex.ru/1.x/ -- for searching the place coordinates

http://api.openweathermap.org/data/2.5/weather -- for getting the weather

https://www.bing.com/images/search -- for searching the image



